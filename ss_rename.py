from PIL import Image
import sys
import glob
import os
import datetime

import pyocr.builders
# import cv2


def main():
    tool, lang = init()

    rename_all(tool, lang)
    # rename_test(tool, lang)


def init():
    tools = pyocr.get_available_tools()
    if len(tools) == 0:
        print("No OCR tool found")
        sys.exit(1)

    tool = tools[0]
    print("Will use tool '%s'" % (tool.get_name()))

    lang = "jpn_new"
    print("Will use lang '%s'" % (lang))

    return tool, lang


def rename_all(tool, lang):
    path = "./img"
    images = glob.glob(path + '/*')

    for img in images:
        txt = img_to_txt(img, tool, lang)
        name, ext = os.path.splitext(img)

        if txt == "":
            last_modified = os.path.getmtime(img)
            mtime = datetime.datetime.fromtimestamp(last_modified)
            txt = mtime.strftime("%d%m%y-%H%M%S")
        if os.path.join(path, txt) == name:
            continue

        os.rename(img, os.path.join(path, txt + ext))
        print("\nrename " + img + " to\n\t" + txt + ext)


def rename_test(tool, lang):
    path = input("Input image name(ex. ./img/test.png\n>> ")
    txt = img_to_txt(path, tool, lang)
    print(txt)


def img_to_txt(path, tool, lang):
    img = Image.open(path)

    left = 0
    upper = img.size[1] - 160
    right = img.size[0]
    lower = img.size[1]

    img = img.crop((left, upper, right, lower))

    '''
    ocv_img = np.asarray(img)
    ocv_img = cv2.cvtColor(ocv_img, cv2.COLOR_BGR2GRAY)
    ocv_img = cv2.GaussianBlur(ocv_img, (5, 5), 0)

    ocv_img = cv2.adaptiveThreshold(ocv_img, 255,
        cv2.ADAPTIVE_THRESH_GAUSSIAN_C,
        cv2.THRESH_BINARY, 11, 2)

    img = Image.fromarray(ocv_img)
    '''

    pixel = img.size
    img2 = Image.new('RGB', pixel)
    for i in range(pixel[0]):
        for j in range(pixel[1]):
            r, g, b = img.getpixel((i, j))
            if r == g == b > 220:
                img2.putpixel((i, j), (0, 0, 0))
            else:
                img2.putpixel((i, j), (255, 255, 255))

    word_boxes = tool.image_to_string(
        img2,
        lang=lang,
        builder=pyocr.builders.LineBoxBuilder(tesseract_layout=6)
    )

    '''
    ocv_img = np.asarray(img2)
    # ocv_img = ocv_img[:, :, ::-1].copy()
    for box in word_boxes:
        print(box.position)
        print(txt)
        cv2.rectangle(ocv_img, box.position[0], box.position[1], (0, 0, 255))
    cv2.imshow('result', ocv_img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
    '''

    txt = ""
    for box in word_boxes:
        txt += box.content\
            .replace(' ', '') \
            .replace('・', '.') \
            .replace('「', '') \
            .replace('」', '') \

    return txt


if __name__ == '__main__':
    main()
